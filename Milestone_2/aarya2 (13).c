#include<stdio.h>
#include<conio.h>
void fibo(int n)
{
    int f1 = 0, f2 = 1, i;

    if (n < 1)
        return;

    for (i = 1; i <= n; i++)
    {
        printf("%d ", f2);
        int next = f1 + f2;
        f1 = f2;
        f2 = next;
    }
}
int main()
{
    int n;
    printf("Enter the position upto which fibonacci series is to be printed: ");
    scanf("%d", &n);
    fibo(n);
    return 0;
}
