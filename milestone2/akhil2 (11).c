#include<stdio.h>
#include<conio.h>
void main()
{
    int n,r,ncr;
    printf("Enter n and r respectively: ");
    scanf("%d %d",&n,&r);
    ncr=fact(n)/(fact(r)*fact(n-r));
    printf("The nCr of %d and %d is %d",n,r,ncr);
}
int fact(int x)
{
    int i=1;
    while(x!=0)
    {
        i=i*x;
        x--;
    }
    return i;
}
